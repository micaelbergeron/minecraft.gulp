import os
import sys
import io
import requests
import argparse
from hashlib import md5

__author__ = 'micael'

def configure():
  parser = argparse.ArgumentParser(description="Download Minecraft backups from a mark2 server.")
  parser.add_argument('baseurl', help='the base url of the server')
  parser.add_argument('backup_path', help='the backups location on the server')
  parser.add_argument('--index', help='the local index file name', default='gulp.md5')
  parser.add_argument('--checksums', help='the remote index file name', default='index.md5')
  parser.add_argument('--outputdir', help='directory where backups are stored', default='.')
  return vars(parser.parse_args())

# Configuration

class Gulp:
  SEPARATOR = "  "
  BUFFER_SIZE = 8192

  config_defaults = {
      'baseurl': "http://mythos.ostigaming.com",
      'backup_path': "osti/backups",
      'checksums': "index.md5",
      'index': "gulp.md5",
      }
  config = {}

  def __init__(self, config):
    for config_key, default in Gulp.config_defaults.items():
      self.config[config_key] = config[config_key] or default

  def __getattr__(self, attribute):
    if attribute in self.config:
      return self.config[attribute]

  def __str__(self):
    return str(self.config)

  def get_backups(self, dir='.'):
    backups = []
    try:
      fs_index = "/".join((dir, self.index))
      backups = [line.strip() for line in io.open(fs_index)]
    except IOError:
      pass
    return backups

  def fetch_checksums(self, dir='.'):
    sys.stdout.write("\n\n== Fetching ==")
    checksum_url = "/".join((self.baseurl, self.backup_path, self.checksums))
    r = requests.get(checksum_url, stream=True)
    if r.status_code == 200:
      new_backups = [line.strip() for line in r.iter_lines()]
      to_download = set(new_backups).difference(self.get_backups())
      if len(to_download) > 0:
        sys.stdout.write("\n New backups are: ")
        for backup in to_download:
          checksum, filename = str(backup).split(Gulp.SEPARATOR)
          sys.stdout.write("\n %s [%s]" % (filename, checksum))
        return to_download
    else:
      raise Exception("Error while fetching metadata, aborting. (status code %s)" % r.status_code)

  def download_backup(self, filename, checksum, dir='.'):
    backup_url = "/".join((self.baseurl, self.backup_path, filename))
    r = requests.get(backup_url, stream=True)
    sys.stdout.write("\n Downloading %s..." % backup_url)
    if r.status_code == 200:
      fs_backup = "/".join((dir, filename))
      with io.open(fs_backup, "w+b") as f:
        for data in r.iter_content(self.BUFFER_SIZE):
          f.write(data)
        sys.stdout.write("Done!")
      return Gulp.SEPARATOR.join((checksum, filename))
    else:
      raise Exception("! Error while downloading backup, aborting. (status code %s)" % r.status_code)

  def write_meta_file(self, backups, dir='.'):
    sys.stdout.write("\n\n== Indexing ==")
    fs_index = "/".join((dir, self.index))
    with io.open(fs_index, "w") as f:
      f.writelines(unicode(backup)+'\n' for backup in backups)
    sys.stdout.write("\n Wrote meta file to %s" % self.index)

  def verify_integrity(self, backups, dir='.'):
    sys.stdout.write("\n\n== Verifying ==")
    for backup in backups:
      checksum, filename = str(backup).split(Gulp.SEPARATOR)
      fs_backup = "/".join((dir, filename))
      try:
        digest = md5(io.open(fs_backup, "rb").read()).hexdigest()
        sys.stdout.write("\n %s =?= %s" % (checksum, digest))
        if str(checksum) != digest:
          sys.stdout.write("\nRemoving corrupted backup %s" % fs_backup)
          os.remove(fs_backup)
          backups.remove(backup)
      except IOError:
        sys.stdout.write("\nMissing backup, removing from index.")
        backups.remove(backup)
    return backups

  def run(self, dir='.'):
    backups = self.get_backups(dir)
    for backup in self.fetch_checksums(dir):
      checksum, filename = str(backup).split(Gulp.SEPARATOR)
      try:
        backups.append(self.download_backup(filename, checksum, dir))
      except Exception as ex:
        print ex.message
    if len(backups) > 0:
      backups = self.verify_integrity(backups, dir)
      self.write_meta_file(backups, dir)
    else:
      sys.stdout.write("\nNo backups have been found :(")


if __name__ == "__main__":
  config = configure()
  print config

  gulp = Gulp(config) 
  gulp.run(config['outputdir'])
